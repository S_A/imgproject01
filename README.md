"Public part of the project - Private part is hidden"

========================================
             R-O-A-D-M-A-P
========================================

[API-SERVER - written in NodeJS]  
- Implement CSRF protection for service POST requests, except API requests  
- Fix Cache Control headers causing troubles with static file cache  
- Add a file model to handle uploaded files  
- Extract DB Connection to a seperate layer  

[CLIENT - written in C++]  

==Windows==  
- Fix upload provider  
- Add a "copy2clipboard" function  

==Ubuntu (Linux)==  
not ready yet

==Mac OSX==  
not ready yet


---

========================================
             API ROUTES
========================================

| URI               | ACTION             |
| ----------------- | ------------------ |
| POST `/api/input` | UPLOAD (Multipart) |  

(more to come)